# Artwork License

The Historical Battlefields Berlin 2140 module for Foundry Virtual Tabletop includes icon artwork licensed from Historical Battlefields, and others. Attributions listed below.

These icons are packaged with and provided for use with this module and may not be redistributed or used outside of Foundry Virtual Tabletop except as permitted by their respective licenses, as noted below.

# Historical Battlefields - Token Builder

Historical Battlefields -- https://www.patreon.com/Grantovich/ -- https://hbtokenbuilder.ru/

Thumbnails and images used with expressed permission by the author.
