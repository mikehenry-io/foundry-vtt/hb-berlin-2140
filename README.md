# Historical Battlefields - Berlin 2140

This battle-map features a post-apocalyptic city from the future, where the new ice age has begun. Different flanking routes and firing positions will give you many options to diversify your sci-fi game!

Thanks to [Historical Battlefields](https://www.patreon.com/Grantovich/) for providing the concept and artwork!
